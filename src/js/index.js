// selector
var menu = document.querySelector('.header--menu-button');

var app = document.querySelector('#app');

// method
function toggleMenu(event) {
    // console.log("toggleMenu: ");
    this.classList.toggle('is-active');
    document.querySelector(".header--nav-menu").classList.toggle("is_active");
    event.preventDefault();
}

// event
menu.addEventListener('click', toggleMenu, false);